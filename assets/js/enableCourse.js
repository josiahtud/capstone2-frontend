// Instantiate a URLSearchParams object so we can execute methods to access specific parts of the query string
let params = new URLSearchParams(window.location.search);

// The "get" method returns the value of the key passed in as an argument
let courseId = params.get('courseId');

let token = localStorage.getItem('token');

fetch(`https://murmuring-scrubland-66627.herokuapp.com/api/courses/${courseId}`, {
    method: 'PUT',
    headers: {
        'Authorization': `Bearer ${token}`
    }
})
.then(res => res.json())
.then(data => {

	console.log(data);

	if(data === true){

		// Enable course successful
	    // Redirect to courses page
	    window.location.replace("./courses.html");

	} else {

	    // Error in enabling a course
	    alert("Something went wrong");

	}

})