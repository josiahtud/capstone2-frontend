let token = localStorage.getItem("token");
// console.log(token);

let profileContainer = document.querySelector("#profileContainer");

let firstName = document.querySelector("#userFirstName");
let lastName = document.querySelector("#userLastName");
let email = document.querySelector("#userEmail");
let mobileNo = document.querySelector("#userMobileNo");


console.log(firstName);
    fetch('https://murmuring-scrubland-66627.herokuapp.com/api/users/details', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
    })
    .then(res => res.json())
    .then(data => {

        console.log(data);

        userFirstName.placeholder = data.firstName;
        userLastName.placeholder = data.lastName;
        userEmail.placeholder = data.email;
        userMobileNo.placeholder = data.mobileNo;
        userFirstName.data = data.firstName;
        userLastName.data = data.lastName;
        userEmail.data = data.email;
        userMobileNo.data = data.mobileNo;

    })

document.querySelector("#editProfile").addEventListener( "submit", (e) => {

    e.preventDefault();

    let userFirstName = firstName.value;
    let userLastName = lastName.value;
    let userEmail = email.value;
    let userMobileNo = mobileNo.value;

    fetch('https://murmuring-scrubland-66627.herokuapp.com/api/users', {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
        body: JSON.stringify({
            firstName: userFirstName,
            lastName: userLastName,
            email: userEmail,
            mobileNo: userMobileNo
        })
    })
    .then(res => res.json())
    .then(data => {
        
        console.log(data);

        if(data === true){

            // Update course successful
            // Redirect to courses page
            window.location.replace("./courses.html");

        }else{

            // Error in updating a course
            alert("something went wrong");

        }

    })

})


// fetch(`http://localhost:4000/api/courses/${courseId}`)
// .then(res => res.json())
// .then(data => {

// 	console.log(data);

// 	// Assign the retrieved data as placeholders and the current value of the input fields
// 	name.placeholder = data.name;
// 	price.placeholder = data.price;
// 	description.placeholder = data.description;
// 	name.value = data.name;
// 	price.value = data.price;
// 	description.value = data.description;

// })

// document.querySelector("#editProfile").addEventListener( "submit", (e) => {

// 	e.preventDefault();

// 	let courseName = name.value;
// 	let courseDescription = description.value;
// 	let coursePrice = price.value;
//     let coursePrice = price.value;

// 	fetch('http://localhost:4000/api/courses', {
//         method: 'PUT',
//         headers: {
//             'Content-Type': 'application/json',
//             'Authorization': `Bearer ${token}`
//         },
//         body: JSON.stringify({
//             courseId: courseId,
//             name: courseName,
//             description: courseDescription,
//             price: coursePrice
//         })
//     })
//     .then(res => res.json())
//     .then(data => {
        
//     	console.log(data);

//     	if(data === true){

//     		// Update course successful
//     	    // Redirect to courses page
//     	    window.location.replace("./courses.html");

//     	}else{

//     	    // Error in updating a course
//     	    alert("something went wrong");

//     	}

//     })

// })