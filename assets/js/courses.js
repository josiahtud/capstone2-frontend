let adminUser = localStorage.getItem("isAdmin");
let cardFooter;
let modalButton = document.querySelector("#adminButton");

if(adminUser == "false" || !adminUser) {

	modalButton.innerHTML = 
	`
		<div class="col-md-2 offset-md-10">
			<a href="./profile.html" class="btn btn-block btn-info">
				Enrolled Courses
			</a>
		</div>
		<div class="col-md-2 offset-md-10">
			<a href="./viewProfile.html" class="btn btn-block btn-info">
				View Profile
			</a>
		</div>
	`


} else {
	modalButton.innerHTML =
	`
		<div class="col-md-2 offset-md-10">
			<a href="./addCourse.html" class="btn btn-block btn-info">
				Add Course
			</a>
		</div>
	`
}

fetch('https://murmuring-scrubland-66627.herokuapp.com/api/courses')
.then(res => res.json())
.then(data => {

	console.log(data);

	// Creates a variable that will store the data to be rendered
	let courseData;

	if(adminUser == "false" || !adminUser) {
		courseData = data.filter(({isActive}) => isActive == true);
		console.log(courseData);
	} else {
		courseData = data;
			}

	if(data.length < 1) {

		courseData = "No courses available"

	} else {

		courseData = courseData.map(courseData => {
 
			if(adminUser == "false" || !adminUser) {

				cardFooter =
					`
						<a href="./course.html?courseId=${courseData._id}" value="${courseData._id}" class="btn btn-secondary text-white btn-block editButton">
							Select Course
						</a>
					`

			} else {

				if(courseData.isActive == true) {
				cardFooter =
					`
						<a href="./editCourse.html?courseId=${courseData._id}" value="${courseData._id}" class="btn btn-info text-white btn-block editButton">
							Edit
						</a>
						<a href="./viewEnrollees.html?courseId=${courseData._id}" value="${courseData._id}" class="btn btn-info text-white btn-block editButton">
							View Enrollees
						</a>
						<a href="./deleteCourse.html?courseId=${courseData._id}" value="${courseData._id}" class="btn btn-danger text-white btn-block deleteButton">
							Disable Course
						</a>
					`
					}	
				else {
				cardFooter =
					`
						<a href="./editCourse.html?courseId=${courseData._id}" value="${courseData._id}" class="btn btn-info text-white btn-block editButton">
							Edit
						</a>
						<a href="./viewEnrollees.html?courseId=${courseData._id}" value="${courseData._id}" class="btn btn-info text-white btn-block editButton">
							View Enrollees
						</a>
						<a href="./enableCourse.html?courseId=${courseData._id}" value="${courseData._id}" class="btn btn-success text-white btn-block deleteButton">
							Enable Course
						</a>

					`
					}	

				}
			

			if(adminUser == "false" || !adminUser) {

			return (

				`
					<div class="col-md-6 my-3">
						<div class="card" style="background-color:mediumturquoise;">
							<div class="card-body">
								<h5 class="card-title">
									${courseData.name}
								</h5>
								<p class="card-text text-left">
									${courseData.description}
								</p>

								<p class="card-text text-right">
									₱ ${courseData.price}
								</p>
							</div>
							<div class="card-footer">
								${cardFooter}
							</div>
						</div>
					</div>
				`
			)

		} else {

			return (

				`
					<div class="col-md-6 my-3">
						<div class="card" style="background-color: powderblue;">
							<div class="card-body">
								<h5 class="card-title">
									${courseData.name}
								</h5>
								<p class="card-text text-left">
									${courseData.description}
								</p>
								<p class="card-text text-left">
									Active: ${courseData.isActive}
								</p>
								<p class="card-text text-right">
									₱ ${courseData.price}
								</p>
							</div>
							<div class="card-footer">
								${cardFooter}
							</div>
						</div>
					</div>
				`
			)


		}



		// Replaces the comma's in the array with an empty string
		}).join("");

		//1st iteration
		/*course = {
			createdOn: "2021-03-02T09:19:40.227Z"
			description: "Let's make our website look cool!"
			enrollees: []
			isActive: true
			name: "CSS"
			price: 500
		}*/

		// 2nd iteration
		/*course = {
			createdOn: "2021-03-02T09:19:40.227Z"
			description: "A basic programming language."
			enrollees: []
			isActive: true
			name: "HTML"
			price: 1000
		}*/



	}
	document.querySelector("#coursesContainer").innerHTML = courseData;
})

// courseData = [

// 	`
// 	<div class="col-md-6 my-3">
// 		<div class="card">
// 			<div class="card-body">
// 				<h5 class="card-title">
// 					CSS
// 				</h5>
// 				<p class="card-text text-left">
// 					Let's make our websites look cooler
// 				</p>
// 				<p class="card-text text-right">
// 					₱ 500
// 				</p>
// 			</div>
// 		</div>
// 	</div>
// 	<div class="col-md-6 my-3">
// 		<div class="card">
// 			<div class="card-body">
// 				<h5 class="card-title">
// 					HTML
// 				</h5>
// 				<p class="card-text text-left">
// 					A basic programming language
// 				</p>
// 				<p class="card-text text-right">
// 					₱ 1000
// 				</p>
// 			</div>
// 		</div>
// 	</div>
// 	`

// ]