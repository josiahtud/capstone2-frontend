console.log(window.location.search);
let params = new URLSearchParams(window.location.search);
console.log(params.has('courseId'));
console.log(params.get('courseId'));

let courseId = params.get('courseId');

let token = localStorage.getItem('token');

let courseName = document.querySelector("#courseName");
let courseDesc = document.querySelector("#courseDesc");
let coursePrice = document.querySelector("#coursePrice");
let enrollContainer=document.querySelector("#enrollContainer");

fetch(`https://murmuring-scrubland-66627.herokuapp.com/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {
	console.log(data);

	courseName.innerHTML = data.name;
	courseDesc.innerHTML = data.description;
	coursePrice.innerHTML = data.price;


	data.enrollees.forEach(userData => {

			console.log(userData);

	fetch(`https://murmuring-scrubland-66627.herokuapp.com/api/users/${userData.userId}`, {
		method: 'GET',
		headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
	})
	.then(res => res.json())
	.then(data => {

				enrollContainer.innerHTML += 
					`<p></p>
						<p>
							<tr>${data.firstName}</tr>
							<tr>${data.lastName}</tr>
						</p>
					`

			})
		
		})

})
