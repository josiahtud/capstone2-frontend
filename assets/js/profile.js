

let token = localStorage.getItem("token");
// console.log(token);

let profileContainer = document.querySelector("#profileContainer");

if(!token || token === null) {

	// Unauthenticated user
	alert('You must login first');
	// Redirect user to login page
	window.location.href="./login.html";

} else {

	fetch('https://murmuring-scrubland-66627.herokuapp.com/api/users/details', {
		method: 'GET',
		headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
	})
	.then(res => res.json())
	.then(data => {

		console.log(data);

		profileContainer.innerHTML = 
			`
				<div class="col-md-12" >
					<section class="jumbotron my-5" style="background-color: powderblue;">		
						<h3 class="text-center">First Name: ${data.firstName}</h3>
						<h3 class="text-center">Last Name: ${data.lastName}</h3>
						<h3 class="text-center">Email: ${data.email}</h3>
						<h3 class="text-center mt-5">Courses Enrolled</h3>
						<table class="table">
							<thead>
								<tr>
									<th> Course ID </th>
									<th> Enrolled On </th>
									<th> Status </th>
								</tr>
							</thead>
							<tbody id="courses">
							</tbody>
						</table> 

					</section>
				</div>
			`

		let courses = document.querySelector("#courses");

		data.enrollments.forEach(courseData => {

			console.log(courseData);

			fetch(`https://murmuring-scrubland-66627.herokuapp.com/api/courses/${courseData.courseId}`)
			.then(res => res.json())
			.then(data => {

				courses.innerHTML += 
					`
						<tr>
							<td>${data.name}</td>
							<td>${courseData.enrolledOn}</td>
							<td>${courseData.status}</td>
						</tr>
					`

			})
		
		})

	})
}